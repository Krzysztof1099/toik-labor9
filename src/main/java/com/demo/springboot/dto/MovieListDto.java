package com.demo.springboot.dto;
import java.util.List;
import java.util.stream.Collectors;

public class MovieListDto {
    private List<MovieDto> movies;

    public MovieListDto(List<MovieDto> movies) {
        this.movies = movies;
    }

    public List<MovieDto> getMovies() {
        return movies;
    }

    public boolean deleteMovie(Integer id){
        for(MovieDto movie :movies){
            if(movie.getMovieId().equals(id)){
                movies.remove(movie);
                return true;
            }
        }
        return false;
    }

    public boolean addMovie(CreateMovieDto createMovieDto){
        movies.add(new MovieDto(movies.size()+1,createMovieDto.getTitle(), createMovieDto.getYear(),createMovieDto.getImage()));
        return true;
    }

    public boolean updateMovie(Integer id, UpdateMovieDto updateMovieDto){
        for(MovieDto movie :movies){
            if(movie.getMovieId().equals(id)){
                movies.set(movies.indexOf(movie), new MovieDto(id,updateMovieDto.getTitle(),updateMovieDto.getYear(),
                        updateMovieDto.getImage()));
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "[" + movies.stream()
                .map(movie -> movie.getMovieId().toString())
                .collect(Collectors.joining(",")) + "]";
    }

}